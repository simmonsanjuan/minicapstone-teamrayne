import React, { useState } from 'react';
import {Redirect, BrowserRouter, Route, Switch} from 'react-router-dom'
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";


//pages

import DamingTimePage from './pages/DamingTimePage';
import CategoryPage from './pages/CategoryPage';
import LandingPage from './pages/LandingPage';
import FrontPage from './pages/FrontPage';


const App = (props) => {
  return(
  	<BrowserRouter>
   		<Switch>
   			<Route component={DamingTimePage} path="/damingtime" />
   			<Route component={CategoryPage} path="/category" />
			<Route component={LandingPage} path="/landing" />
			<Route component={FrontPage} exact path="/" />
    	</Switch>
   	</BrowserRouter>
  )
}

export default App;
import React, {Fragment} from 'react';
import {Container, Row, Col, Button} from 'reactstrap';


const LandingPage = (props) => {
	return(
		<Fragment>
		<Container className = "my-5">
			<Row className = "mb-3">
				<Col>
					<h1 className="text-center">Welcome, </h1>
				</Col>
			</Row>
		</Container>

		<Container className = "my-5">
			<Row className = "mb-3 sm-6">
				<Col className = "btnSimmon">
					<Button className="btn1"> Daming Time ? </Button>
				</Col>
				<Col className = "btnRayne sm-6">
					<Button className="btn1 btn-primary ml-1">Walang Time?</Button>
				</Col>
			</Row>
		</Container>

		</Fragment>
	)
}

export default LandingPage;
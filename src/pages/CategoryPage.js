import React, {Fragment} from 'react';
import {Button, Form, FormGroup, Label, Input, FormText, Container, Col, Row} from 'reactstrap';



const CategoryPage = (props) => {
	return (
		<Fragment>
			<Container className = "my-5">
				<Row className = "mb-3">
					<Col>
						<h1 className="text-center mt-5">Choose a Category:</h1>
					</Col>
				</Row>
			</Container>

			<Container className="ml-auto">
				<Row className = "mb-3">
					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1"> IN LOVE</Button>
					</Col>

					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1">HUNGRY</Button>
					</Col>

					<Col className = "md-4">
						<Button className="btnCategory btn-primary ml-1">PAGOD</Button>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}

export default CategoryPage;
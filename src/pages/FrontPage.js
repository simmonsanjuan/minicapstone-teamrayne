import React, {Fragment} from 'react';
import {Button, Form, FormGroup, Label, Input, FormText, Container, Col, Row} from 'reactstrap';

const FrontPage = (props) => {

	return(
		<Fragment>
			<Container className = "my-5">
				<Row className = "mb-3">
					<Col>
						<h1 className="text-center mt-5">What is your Nickname?</h1>
					</Col>
				</Row>
			</Container>

				<Container className = "my-5 md-6">
					<Form>
						<FormGroup>
							<Label for="nickname" className="mt-3">Nickname:</Label>
							<Input className="text-center" type="text" name="nickname" id="nickname" placeholder="Input your nickname" />
						</FormGroup>
					</Form>
		 				<button className="btn2 btn-primary ml-1">Submit</button>
				</Container>
		</Fragment>
	)
}

export default FrontPage;


	
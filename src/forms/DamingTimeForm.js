import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';


const DamingTimeForm = (props) => {
	return(
		<Form>

			<FormGroup>
				<Label for="title" className="mt-3">Title</Label>
        		<Input type="text" name="title" id="title" placeholder="with a placeholder" />
			</FormGroup>

			<FormGroup>
				<Label for="description" className="mt-3">Description:</Label>
        		<Input type="text" name="description" id="description" placeholder="with a placeholder" />
			</FormGroup>

			<FormGroup>
				<Label for="image" className="mt-3">Image:</Label>
        		<Input type="file" name="image" id="image" placeholder="with a placeholder" />
			</FormGroup>
				<Button className="btn-block border">Save File</Button>
		</Form>
	)
}

export default DamingTimeForm;
